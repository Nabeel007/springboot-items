package com.items.controller;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.items.entity.Items;
import com.items.service.ItemService;
import com.items.vo.Response;

@RestController
public class ItemsController {

	@Autowired
	ItemService items;

	@GetMapping(value = "/rest/items", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> getItems() {

		Response response = new Response();
		List<Items> listItems;
		try {
			listItems = items.getItemsFromDB();
			List<Items> finalresult = listItems.stream()
	        
					 .sorted(Comparator.comparing(Items::getPrice).reversed())
					 .collect(Collectors.toList());	
			response.setListItems(finalresult);

			return new ResponseEntity<Response>(response, HttpStatus.ACCEPTED);
		}

		catch (NullPointerException e) {
			return new ResponseEntity<Response>(response, HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity<Response>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

}
