package com.items.vo;

import java.util.List;

import com.items.entity.Items;

public class Response {
	
	List<Items> listItems;

	public List<Items> getListItems() {
		return listItems;
	}

	public void setListItems(List<Items> listItems) {
		this.listItems = listItems;
	}
	
	

}
