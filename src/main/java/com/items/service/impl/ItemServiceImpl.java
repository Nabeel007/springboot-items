package com.items.service.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;

import com.items.entity.Items;
import com.items.service.ItemService;

@Service
public class ItemServiceImpl implements ItemService {
	
	@PersistenceContext
    private EntityManager entityManager;


	@Override
	public List<Items> getItemsFromDB() throws NullPointerException,Exception {
		
		
		Query query = entityManager.createQuery("SELECT i FROM Items i");
		List<Items> itemsList =query.getResultList();

		return itemsList;
	}

}
