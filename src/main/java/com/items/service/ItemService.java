package com.items.service;

import java.util.List;

import com.items.entity.Items;

public interface ItemService {
	
	List <Items>getItemsFromDB() throws NullPointerException, Exception;

}
