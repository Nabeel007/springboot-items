package com.items.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Items {
	
	@Id
	private int id;
	private float price;
	private String description;
	private  String brand;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	
	
	
	

}
